<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />

		<title>Contact us,leave feedback successful report page</title>
		<meta name="description" content="Feedback PHP page using form to be filled by users ,sharing their experience from our site and their suggestions to make our content better and more accessible" />
		<meta name="author" content="Yuliyan Yordanov" />

		<meta name="viewport" content="width=device-width; initial-scale=1.0" />
		<link href='http://fonts.googleapis.com/css?family=Cantarell:400,400italic,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="styles.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="imgs/favicon.ico" />
		<link rel="icon" type="image/gif" href="imgs/animated_uPC_logo.gif" />
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-33224309-1', 'hourb.com');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');

	
	document.createElement("article");
	document.createElement("footer");
	document.createElement("header");
	document.createElement("hgroup");
	document.createElement("nav");
	document.createElement("aside");
	
</script>		
		
	</head>
<body itemscope itemtype="http://schema.org/WebPage">
	<header>
		<span class="head1"><a href="index.html">ULTIMATE PC</a></span> <span class="head2">the computer experts that speak your language</span>
		<div class="mobileinfo"><p>mobile version</p></div>
	</header>
	<div class="accessaid">Main menu</div>
	<nav>
		<ul id="menu">
		    <li id="home"><a href="index.html">home</a>   
		    </li>
		    <li id="casual"><a href="casual.html">casual user</a> 
		    </li>
		    <li id="advanced"><a href="advanced.html">advanced</a> 
		    </li>
		    <li id="gamer"><a href="gamer.html">gamer</a> 
		    </li>
			 <li id="howto"><a href="howto.html">how-to</a>
		    		<ul class="submenu">
		    			<li><a href="buildpc/buildpc.html">Assemble a PC</a></li>
		    			<li><a href="speedup/speedup.html">Speed up a PC</a></li>
		    		</ul>
		    </li>
			 <li id="feedback"><a href="feedback.html">feedback</a>
		    </li>
		</ul>
		<!-- g+1 button -->
  				<div class="g-plusone" data-size="medium"></div>
				<script type="text/javascript">
				  (function() {
				    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				    po.src = 'https://apis.google.com/js/plusone.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
				  })();
				</script>
				<div class="social">
					<a class="fb" href="http://www.facebook.com/sharer.php?u=http://ultimatepc.hourb.com">
					Share This Link on Facebook</a>
					<a class="tw" href="http://twitter.com/share?text=An%20Awesome%20Desktop%20Computer%20Guide&amp;url=http://ultimatepc.hourb.com">
					Share This on Twitter</a>
					<a class="ln" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://ultimatepc.hourb.com">
					Share This on LinkedIn</a>
					
				</div>
		<ul id="breadcrumbs" itemprop="breadcrumb">
			<li class="first"><a href="index.html">Home</a></li>
			<li> &gt;&gt; <a href="feedback.html">Feedback</a></li>
			<li> &gt;&gt; <em>Submition report</em></li>
		</ul>
	</nav>

<?php
//This is a very simple PHP script that outputs the name of each bit of information (that corresponds to the <code>name</code> attribute for that field) along with the value that was sent with it right in the browser window, and then sends it all to an email address (once you've added it to the script).
$email=$_POST['email'];
$name=$_POST['name'];

	if (empty($_POST)) {
		print "<p>No data was submitted.</p>";
		print "</body></html>";
		exit();
}

//Creates function that removes magic escaping, if it's been applied, from values and then removes extra newlines and returns to foil spammers. Thanks Larry Ullman!
	function clear_user_input($value) {
		if (get_magic_quotes_gpc()) $value=stripslashes($value);
		$value= str_replace( "\n", '', trim($value));
		$value= str_replace( "\r", '', $value);
		return $value;
		}


//if ($_POST['comments'] == 'Please share any comments you have here') $_POST['comments'] = '';	

//-------------------------------------------------------------------------------------------------//

//--------------------------------------------------------------------------------------------------//

$body ="Here is the data that was submitted:\n";

	foreach ($_POST as $key => $value) {
		$key = clear_user_input($key);
		$value = clear_user_input($value);
		if ($key=='components') {
			
		if (is_array($_POST['components']) ){
			$body .= "$key: ";
			$counter =1;
			foreach ($_POST['components'] as $value) {
					//Add comma and space until last element
					if (sizeof($_POST['components']) == $counter) {
						$body .= "$value\n";
						break;}
					else {
						$body .= "$value, ";
						$counter += 1;
						}
					}
			} else {
			$body .= "$key: $value\n";
			}
		} else {
	
		$body .= "$key: $value\n";
		}
	}
	


extract($_POST);
//removes newlines and returns from $email and $name so they can't smuggle extra email addresses for spammers
$email = clear_user_input($email);
$name = clear_user_input($name);

//Create header that puts email in From box along with name in parentheses and sends bcc to alternate address
$from='From: '. $email . "(" . $name . ")" . "\r\n" . 'Bcc: yuliyan-yordanov@hotmail.com' . "\r\n";

$name=$_REQUEST['name'];

//Creates intelligible subject line that also shows me where it came from
$subject = 'Feedback from user';

//Sends mail to me, with elements created above
mail ('yuliyan-yordanov@hotmail.com', $subject, $body, $from);
 
//echo "<p>Dear <b>$name</b> ,thank you for your feedback!</p>" ;    //new change
?>
	<div class="sreport">
	<h1>Submision report</h1>
	<p><em>Thank you!</em> Feedback submition <span class="orange"><em>successful</em></span> ! Feel free to browse the site again or go back to<a href="feedback.html"> Feedback page</a>.</p>
	</div>
	<footer>
				<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="paypal">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="HFSSLGQRLR54Y">
				<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
				<img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
				</form>		
			<div id="footnav">
					<ul id="first">
					    <li><a href="index.html">Home</a>   
					    </li>
					</ul>
					<ul id="second">
					    <li><a href="casual.html">Casual user</a> 
					    </li>
					    <li><a href="advanced.html">Advanced</a> 
					    </li>
					    <li><a href="gamer.html">Gamer</a> 
					    </li>
					</ul>
					<ul id="third">
						 <li><a href="howto.html">How-to</a>
						 </li>
					    <li><a href="buildpc/buildpc.html">Assemble a PC</a>
					    </li>
					    <li><a href="speedup/speedup.html">Speed up a PC</a>
					    </li>
					</ul>
					<ul id="fourth">
					    <li><a href="feedback.html">Feedback</a>
					    </li>
					    <li><a href="sitemap.html">Sitemap</a>
					    </li>
	  				</ul>
				</div>
		<p>	&copy; Copyright  by Yuliyan Yordanov 2012	</p>
	</footer>

</body>
</html>
