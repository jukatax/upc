<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />

		<title>Thank you for donating</title>
		<meta name="description" content="Desktop computer configurations for gamers- components explained for ultimate gaming PC performance" />
		<meta name="author" content="Yuliyan Yordanov" />
		<meta name="keywords" content="gamer,PC builds,gamer PC builds,budget gamer builds,ultimate PC,nVidia,Intel,AMD,Radeon" />
		<meta name="viewport" content="width=device-width; initial-scale=1.0" />
		<link href='http://fonts.googleapis.com/css?family=Cantarell:400,400italic,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="styles.css" type="text/css" media="screen"  />
		<link rel="shortcut icon" href="imgs/favicon.ico" />
		<link rel="icon" type="image/gif" href="imgs/animated_uPC_logo.gif" />
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-33224309-1', 'hourb.com');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
	document.createElement("article");
	document.createElement("footer");
	document.createElement("header");
	document.createElement("hgroup");
	document.createElement("nav");
	document.createElement("aside");
</script>		
		
	</head>

	<body itemscope itemtype="http://schema.org/WebPage">
		
			<header>
				<span class="head1"><a href="index.html">ULTIMATE PC</a></span> <span class="head2">the computer experts that speak your language</span>
				<div class="mobileinfo"><p>mobile version</p></div>
			</header>
			<div class="accessaid">Main menu</div>
			<nav>
				<ul id="menu">
				    <li id="home"><a href="index.html">Home</a>   
				    </li>
				    <li id="casual"><a href="casual.html">casual user</a> 
				    </li>
				    <li id="advanced"><a href="advanced.html">advanced</a> 
				    </li>
				    <li id="gamer"><a href="gamer.html">gamer</a> 
				    </li>
					 <li id="howto"><a href="howto.html">how-to</a>
				    		<ul class="submenu">
				    			<li><a href="buildpc/buildpc.html">Assemble a PC</a></li>
				    			<li><a href="speedup/speedup.html">Speed up a PC</a></li>
				    		</ul>
				    </li>
					 <li id="feedback"><a href="feedback.html">feedback</a>
				    </li>
				</ul>
				<!-- g+1 button -->
  				<div class="g-plusone" data-size="medium"></div>
				<script type="text/javascript">
				  (function() {
				    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				    po.src = 'https://apis.google.com/js/plusone.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
				  })();
				</script>
				<div class="social">
					<a class="fb" href="http://www.facebook.com/sharer.php?u=http://ultimatepc.hourb.com">
					Share This Link on Facebook</a>
					<a class="tw" href="http://twitter.com/share?text=An%20Awesome%20Desktop%20Computer%20Guide&amp;url=http://ultimatepc.hourb.com">
					Share This on Twitter</a>
					<a class="ln" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://ultimatepc.hourb.com">
					Share This on LinkedIn</a>
					
				</div>
				<ul id="breadcrumbs" itemprop="breadcrumb">
  					<li class="first"><a href="index.html">Home</a></li>
  					<li> &gt;&gt; <em>Thank You</em></li>
  				
  				</ul>
			</nav>

			<div class="left" itemprop="mainContentOfPage">
				<h1>Thank You for donating</h1>
				<p>Thank You for your kind donation. We work hard to search and deliver the best content for people that need it!</p>
				
			</div>	
			
			<aside class="right" style="display:none;">
 				<div class="leftm">
 					<h3>Suggested PC components</h3>
								  <ul class="pc-config">
									  <li>Processor
										  <ul>
											  <li><em>Intel Core i5 4670</em></li>
											  <li><em>Intel Core i5-4670K</em></li>
											  <li><em>Intel Core i7-4770K</em></li>
											  <li><em>AMD FX 8150</em></li>
										  </ul>
									  </li>	
									  <li>Graphics
										  <ul>
											  <li><em>AMD Radeon HD 7850</em></li>
											  <li><em>nVidia GeForce GTX660</em></li>
											  <li><em>nVidia GeForce GTX670</em></li>
											  <li><em>nVidia GeForce GTX680</em></li>
											  <li><em>nVidia GeForce GTX770</em></li>
											  <li><em>nVidia GeForce GTX780</em></li>
										  </ul>
									  </li>
									  <li>Memory
										  <ul>
											  <li><em>8 GB</em></li>
											  <li><em>16 GB</em></li>
										  </ul>
									  </li>
									  <li>Hard disk
										  <ul>
											  <li><em>SSD 128&#47;256 GB</em></li>
											  <li><em>Seagate 1TB,2TB or 3TB</em></li>
											  <li><em>WD 1TB,2TB or 3TB</em></li>
										  </ul>
									  </li>
								  </ul> 
				</div>
 				<div class="rightm">
 					<h3>Compare prices</h3>
								  <ul class="pc-config-lnk">
									  <li><a href="http://www.amazon.co.uk/" >Amazon.co.uk</a></li>
									  <li><a href="http://www.dabs.com/" >Dabs.com</a></li>
									  <li><a href="http://www.ebuyer.com/" >Ebuyer.com</a></li>
									  <li><a href="http://www.scan.co.uk/" >Scan.co.uk</a></li>
								  </ul>
				</div>
 			</aside>
			
			<footer>
				<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" class="paypal">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="HFSSLGQRLR54Y">
				<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
				<img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
				</form>
				<div id="footnav">
					<ul id="first">
					    <li><a href="index.html">Home</a>   
					    </li>
					</ul>
					<ul id="second">
					    <li><a href="casual.html">Casual user</a> 
					    </li>
					    <li><a href="advanced.html">Advanced</a> 
					    </li>
					    <li><a href="gamer.html">gamer</a> 
					    </li>
					</ul>
					<ul id="third">
						 <li><a href="howto.html">How-to</a>
						 </li>
					    <li><a href="buildpc/buildpc.html">Assemble a PC</a>
					    </li>
					    <li><a href="speedup/speedup.html">Speed up a PC</a>
					    </li>
					</ul>
					<ul id="fourth">
					    <li><a href="feedback.html">Feedback</a>
					    </li>
					    <li><a href="sitemap.html">Sitemap</a>
					    </li>
	  				</ul>
				</div>
				<p>	&copy; Copyright  by Yuliyan Yordanov 2012	</p>
			</footer>
		
	</body>
</html>
