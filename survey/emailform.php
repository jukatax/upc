<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />

		<title>FEEDBACK COMPLETION PAGE</title>
		<meta name="description" content="Feedback PHP page using form to be filled by users ,sharing their experience from our site and their suggestions to make our content better and more accessible" />
		<meta name="author" content="Yuliyan Yordanov" />

		<meta name="viewport" content="width=device-width; initial-scale=1.0" />
		<link href='http://fonts.googleapis.com/css?family=Cantarell:400,400italic,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="styles.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="imgs/favicon.ico" />
		<link rel="icon" type="image/gif" href="imgs/animated_uPC_logo.gif" />
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		
	</head>
<body>
	<header>
		<hgroup><h1>ULTIMATE PC</h1> <h2>the computer experts that speak your language</h2></hgroup>
	</header>
	<nav>
		<ul id="menu">
		    <li id="home"><a title="Go to Home page" href="index.html">home</a>   
		    </li>
		    <li id="casual"><a title="Go to the Casual user's page" href="casual.html">casual user</a> 
		    </li>
		    <li id="advanced"><a title="Go to the Advanced user's page" href="advanced.html">advanced</a> 
		    </li>
		    <li id="gamer"><a title="Go to the Gamer's page" href="gamer.html">gamer</a> 
		    </li>
			 <li id="howto"><a title="Go to the How-to page" href="howto.html">how-to</a>
		    		<ul class="submenu">
		    			<li><a title="Go to the Assemble a PC page" href="buildpc/buildpc.html">Assemble a PC</a></li>
		    			<li><a title="Go to the Speed up a PC page" href="speedup/speedup.html">Speedup a PC</a></li>
		    		</ul>
		    </li>
			 <li id="feedback"><a title="Go to the Feedback page" href="feedback.html">feedback</a>
		    </li>
		</ul>
		<ul id="breadcrumbs">
			<li class="first"><a href="index.html"></a></li>
			<li> &gt;&gt; <a href="feedback.html">Feedback</a></li>
			<li> &gt;&gt; <em>Submition report</em></li>
		</ul>
	</nav>

<?php
//This is a very simple PHP script that outputs the name of each bit of information (that corresponds to the <code>name</code> attribute for that field) along with the value that was sent with it right in the browser window, and then sends it all to an email address (once you've added it to the script).
$email=$_POST['email'];
$name=$_POST['name'];

	if (empty($_POST)) {
		print "<p>No data was submitted.</p>";
		print "</body></html>";
		exit();
}

//Creates function that removes magic escaping, if it's been applied, from values and then removes extra newlines and returns to foil spammers. Thanks Larry Ullman!
	function clear_user_input($value) {
		if (get_magic_quotes_gpc()) $value=stripslashes($value);
		$value= str_replace( "\n", '', trim($value));
		$value= str_replace( "\r", '', $value);
		return $value;
		}


//if ($_POST['comments'] == 'Please share any comments you have here') $_POST['comments'] = '';	

//-------------------------------------------------------------------------------------------------//

//--------------------------------------------------------------------------------------------------//

$body ="Here is the data that was submitted:\n";

	foreach ($_POST as $key => $value) {
		$key = clear_user_input($key);
		$value = clear_user_input($value);
		if ($key=='components') {
			
		if (is_array($_POST['components']) ){
			$body .= "$key: ";
			$counter =1;
			foreach ($_POST['components'] as $value) {
					//Add comma and space until last element
					if (sizeof($_POST['components']) == $counter) {
						$body .= "$value\n";
						break;}
					else {
						$body .= "$value, ";
						$counter += 1;
						}
					}
			} else {
			$body .= "$key: $value\n";
			}
		} else {
	
		$body .= "$key: $value\n";
		}
	}
	


extract($_POST);
//removes newlines and returns from $email and $name so they can't smuggle extra email addresses for spammers
$email = clear_user_input($email);
$name = clear_user_input($name);

//Create header that puts email in From box along with name in parentheses and sends bcc to alternate address
$from='From: '. $email . "(" . $name . ")" . "\r\n" . 'Bcc: yuliyan-yordanov@hotmail.com' . "\r\n";

$name=$_REQUEST['name'];

//Creates intelligible subject line that also shows me where it came from
$subject = 'Feedback from user';

//Sends mail to me, with elements created above
mail ('yuliyan-yordanov@hotmail.com', $subject, $body, $from);
 
echo "<p>Dear <b>$name</b> ,thank you for your feedback!</p>" ;    //new change
?>

	<p>Go back to<a href="feedback.html"> Feedback page</a></p>
	
	<footer>
			<div id="footnav">
				<ul id="first">
				    <li><a title="Go to Home page"  href="index.html">Home</a>   
				    </li>
				</ul>
				<ul id="second">
				    <li><a title="Go to the Casual user's page" href="casual.html">Casual user</a> 
				    </li>
				    <li><a title="Go to the Advanced user's page"  href="advanced.html">Advanced</a> 
				    </li>
				    <li><a title="Go to the Gamer's page" href="gamer.html">Gamer</a> 
				    </li>
				</ul>
				<ul id="third">
					 <li><a title="Go to the How-to page" href="howto.html">How-to</a>
					 </li>
				    <li><a title="Go to the Assemble a PC page" href="buildpc/buildpc.html">Assemble a PC</a>
				    </li>
				    <li><a title="Go to the Speed up a PC page" href="speedup/speedup.html">Speedup a PC</a>
				    </li>
				</ul>
				<ul id="fourth">
				    <li><a title="Go to the Feedback page" href="feedback.html">Feedback</a>
				    </li>
  				</ul>
			</div>	
		<p>	&copy; Copyright  by Yuliyan Yordanov 2012	</p>
	</footer>

</body>
</html>
